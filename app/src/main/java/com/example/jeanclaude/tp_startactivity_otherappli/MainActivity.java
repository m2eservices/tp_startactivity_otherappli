package com.example.jeanclaude.tp_startactivity_otherappli;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.SearchManager;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // exemple pour trouver une application sur le Play Store
        // pour le market, compte gmail obligatoire dans le téléphone
        Button btn_boite_coucou = (Button) findViewById(R.id.btn_boite_coucou);
        btn_boite_coucou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goMarket();
            }
        });

        // exemple pour rechercher une application sur le Play Store
        // pour le market, compte gmail obligatoire dans le téléphone
        Button btn_coucou = (Button) findViewById(R.id.btn_coucou);
        btn_coucou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchMarket();
            }
        });


        // exemple pour appeler qqn
        Button btn_AppelTel = (Button) findViewById(R.id.btn_appelTel);
        btn_AppelTel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doAppel();
            }
        });


        // exemple pour itinéraire Google Maps
        Button btn_itineraire = (Button) findViewById(R.id.btn_itineraire);
        btn_itineraire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doItineraire();
            }
        });


        // exemple pour un navigateur web
        Button btn_WebBrowser = (Button) findViewById(R.id.btn_WebBrowser);
        btn_WebBrowser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doWebBrowser();
            }
        });


        // exemple pour une recherche sur le web
        Button btn_WebResearch = (Button) findViewById(R.id.btn_WebResearch);
        btn_WebResearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doWebResearch();
            }
        });


        // exemple pour une géoloc
        Button btn_geoLoc = (Button) findViewById(R.id.btn_geoLoc);
        btn_geoLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doGeoLoc();
            }
        });
    }


    // exemple pour trouver une application sur le Play Store
    private void doAppel() {
        Uri uri = Uri.parse("tel:0612345678");
        Intent intent = new Intent(Intent.ACTION_DIAL, uri);

        //Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        // pour appeler (si permission indiquée !) :
        // <uses-permission android:name="android.permission.CALL_PHONE"/>
        // Intent intent = new Intent(Intent.ACTION_CALL,uri);
        startActivity(intent);
    }

    // exemple pour rechercher une application sur le Play Store
    private void goMarket() {
        Uri uri = Uri.parse("market://details?id=fr.univ_lille_1.ieea.jc_tarby.boite_a_coucou");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);

        // ou encore
//         Intent intent = new Intent(Intent.ACTION_VIEW);
//         intent.setData(Uri.parse("market://details?id=com.andrilex.barakafrit.activities"));
//         startActivity(intent);
    }

    private void searchMarket() {
        Uri uri = Uri.parse("market://search?q=boite coucou");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    private void doWebBrowser() {
        Uri uri = Uri.parse("http://www.google.fr/");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    private void doWebResearch() {
        Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
        intent.putExtra(SearchManager.QUERY, "Laboratoire CRIStAL lille");
        startActivity(intent);
    }

    private void doGeoLoc() {
        // Map point based on address
//		 Uri location =
//		 Uri.parse("geo:0,0?q=Cite+Scientifique,+Villeneuve-d'Ascq,+France");
        // Or map point based on latitude/longitude
        Uri location = Uri.parse("geo:50.611921,3.142537?z=17");
        // z param is zoom level
        //Uri location = Uri.parse("geo:0,0?q=paris");
        Intent intent = new Intent(Intent.ACTION_VIEW, location);
        startActivity(intent);
    }

    private void doItineraire() {
        // on veut afficher l'itinéraire de Paris à Lille
        String url = "http://maps.google.com/maps?saddr=Paris&daddr=Lille";
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }
}
